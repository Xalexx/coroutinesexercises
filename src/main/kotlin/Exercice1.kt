import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
The main program is started
The main program continues
Background processing started
1s
Background processing finished
0.5s
The main program is finished
*/

fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}
