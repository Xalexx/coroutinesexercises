import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
The main program is started
The main program continues
Background processing started
0.5s
The main program is finished


- El runBlocking bloquea el hilo principal hasta que no finaliza su ejecucion, por lo tanto como el delay es inferior a
la coroutine global, acaba el run blocking pero no se ejecuta el ultimo print del globalScout ya que despues del
runBlocking finaliza el hilo principal.
*/

fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(500)
        println("The main program is finished")
    }
}