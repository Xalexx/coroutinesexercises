import kotlinx.coroutines.*
import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)
    println("Introduce el numero de veces que deseas imprimir 'Hello World!': ")
    val userInput = sc.nextInt()
    printerV1(userInput)
    println("Finished!")
}

fun printerV1(times: Int) {
    for (i in 1..times){
        runBlocking {
            launch {
                println("$i: Hello World!")
                delay(1000)
            }
        }

    }
}