import kotlinx.coroutines.*
import java.util.*

val sc = Scanner(System.`in`)

fun main() {
    val secretNumber = (1..50).random()
    var userGuess: Int?
    var countdown = 10

    CoroutineScope(Dispatchers.Default).launch {
        for (i in 10 downTo 1){
            delay(1000)
            countdown--
        }
    }
    println("You have 10 seconds to guess the secret number...")
    while (true) {
        if (countdown == 0) {
            println("The time is up! The secret number was: $secretNumber")
            break
        }
        print("Enter a number: ")
        userGuess = sc.nextInt()
        // Comprobar si el valor introducido es correcto
        if (userGuess == secretNumber) {
            println("You got it!")
            break
        } else println("Wrong number!")
    }
}