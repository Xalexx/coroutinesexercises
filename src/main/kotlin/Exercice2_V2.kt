import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*

fun main() {
    val sc = Scanner(System.`in`)
    println("Introduce el numero de veces que deseas imprimir 'Hello World!': ")
    val userInput = sc.nextInt()
    runBlocking {
        launch {
            printerV2(userInput)
        }
    }
    println("Finished!")
}

suspend fun printerV2(times: Int) {
    for (i in 1..times){
        println("$i: Hello World!")
        delay(1000)
    }
}