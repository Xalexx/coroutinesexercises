import kotlinx.coroutines.*

/*
The main program is started
Background processing started
1s
Background processing finished
The main program continues
0.5s
The main program is finished.
*/

suspend fun main() {
    println("The main program is started")
    withContext(Dispatchers.IO) {
        launch {
            doBackgroundV2()
        }
    }
    println("The main program continues")
    runBlocking {
        delay(500)
        println("The main program is finished")
    }
}

suspend fun doBackgroundV2(){
    println("Background processing started")
    delay(1000)
    println("Background processing finished")
}