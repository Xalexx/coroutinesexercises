import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
The main program is started
The main program continues
Background processing started
0.5s
The main program is finished


- Pasa lo mismo ya que simplemente estoy llevando el codigo dentro de una funcion y la estoy llamando en el mismo sitio.
*/

fun main() {
    println("The main program is started")
    GlobalScope.launch {
        doBackgroundV1()
    }
    println("The main program continues")
    runBlocking {
        delay(500)
        println("The main program is finished")
    }
}

suspend fun doBackgroundV1(){
    println("Background processing started")
    delay(1000)
    println("Background processing finished")
}