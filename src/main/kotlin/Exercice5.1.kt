import kotlinx.coroutines.*

fun main() {
    runBlocking {
        val firstCor = async {
             corroutine1()
        }
        val secondCor = async {
            corroutine2()
        }
        firstCor.await()
        secondCor.await()
    }
    println("Completed")
}

suspend fun corroutine1() {
    println("Hello World 1.1")
    delay(3000)
    println("Hello World 1.2")
    delay(3000)
    println("Hello World 1.3")
}

suspend fun corroutine2() {
    println("Hello World 2.1")
    delay(2000)
    println("Hello World 2.2")
    delay(2000)
    println("Hello World 2.3")
}
//FONT https://stackoverflow.com/questions/57457079/run-two-kotlin-coroutines-inside-coroutine-in-parallel