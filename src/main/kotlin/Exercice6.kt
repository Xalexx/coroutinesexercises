import kotlinx.coroutines.*

const val redColor = "\u001b[31m"
const val resetColor = "\u001b[0m"

suspend fun main() {
    val horsePosition = mutableListOf<Int>()
    runBlocking {
        val firstHorse = async {
            for (i in 1..4) {
                delay(getRandomDelay())
                println("El caballo 1 esta en el punt $i del circuito")
            }
            println("El${redColor} caballo 1 ha acabado la carrera!!!${resetColor}")
            horsePosition.add(1)
        }
        val secondHorse = async {
            for (i in 1..4) {
                delay(getRandomDelay())
                println("El caballo 2 esta en el punt $i del circuito")
            }
            println("El${redColor} caballo 2 ha acabado la carrera!!!${resetColor}")
            horsePosition.add(2)
        }
        val thirdHorse = async {
            for (i in 1..4) {
                delay(getRandomDelay())
                println("El caballo 3 esta en el punt $i del circuito")
            }
            println("${redColor}El caballo 3 ha acabado la carrera!!!$${resetColor}")
            horsePosition.add(3)
        }
        val fourHorse = async {
            for (i in 1..4) {
                delay(getRandomDelay())
                println("El caballo 4 esta en el punt $i del circuito")
            }
            println("\n${redColor}El caballo 4 ha acabado la carrera!!!${resetColor}")
            horsePosition.add(4)
        }
        firstHorse.await()
        secondHorse.await()
        thirdHorse.await()
        fourHorse.await()
    }
    coroutineScope {
        launch {
            delay(2000)
            println("\n\nRanking\n____________")
            for (i in horsePosition.indices){
                println("Posición ${i+1}: Caballo ${horsePosition[i]}")
            }
        }
    }
}

private fun getRandomDelay(): Long {
    return (1000L..10000L).random()
}