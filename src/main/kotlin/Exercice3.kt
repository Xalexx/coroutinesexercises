import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.io.path.Path
import kotlin.io.path.readLines
import kotlin.system.measureTimeMillis

val file = File("./src/main/kotlin/song")

suspend fun main() {
    val lines = file.readLines()
    val list = mutableListOf<List<String>>()
    for (i in lines.indices){
        list.add(lines[i].split(" "))
    }
    val time = measureTimeMillis {
        for (i in list.indices){
            for (j in list[i].indices){
                coroutineScope {
                    launch {
                        if (list[i][j] != ";") print("${list[i][j]} ")
                        delay(200)
                    }
                }
            }
            println()
        }
    }
    println("\nLa duracion de la reproduccion de la cancion: ${time/1000}s")
}
